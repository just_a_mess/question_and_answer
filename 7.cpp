#include <cstdio>
bool ispr(int x) {
	if (x <= 1) return false;
	for (int i = 2; i*i <= x; i++) if (x%i == 0) return false;
	return true;
}
int main() {
	int p[125], c = 0, f = 0;
	for (int i = 2; i < 100 / 2; i++) if (ispr(i)) p[c++] = i;
	for (int i = 6; i <= 100; i+=2) {
		for (int j = 0; j < c && p[j] <= i/2; j++) 
			if (ispr(i-p[j])) {
				printf("%d=%d+%d", i, p[j], i-p[j]);
				break;
			}
		if (++f % 5 && i < 100) printf(",");
		else printf("\n");
	}
	return 0;
}