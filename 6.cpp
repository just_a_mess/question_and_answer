#include <cstdio>
#include <cstring>
#include <climits>
int main() {
	char word[128] = {0}, ch, mxwd[128] = {0}, mnwd[128] = {0};
	int mxlen = 0, mnlen = INT_MAX, len = 0;
	bool st = false;
	while ((ch = getchar()) != '\n') {
		if (ch == ' ' || ch == ',') {
			if (st && len > 0) {
				if (mxlen < len) strcpy(mxwd, word);
				if (mnlen > len) strcpy(mnwd, word);
				mxlen = mxlen < len ? len : mxlen;
				mnlen = mnlen > len ? len : mnlen;
				len = 0;
				memset(word, 0, sizeof(word));
			}
			continue;
		}
		word[len++] = ch;
		st = true;
	}
	if (mxlen < len) strcpy(mxwd, word);
	if (mnlen > len && len > 0) strcpy(mnwd, word);
	printf("%s\n%s", mxwd, mnwd);
	return 0;
}
