#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
struct student {
	int id;
	string name;
	int scorem, scorep, scorec; // Math, Physics, C++
	int total;
} stu[40];
int amount = 0;
bool cmp(const student& stu1, const student& stu2) {
	return stu1.total > stu2.total;
}
void input() {
	int sub = 0;
	while (cin >> stu[sub].id >> stu[sub].name >> stu[sub].scorem
			>> stu[sub].scorep >> stu[sub].scorec) {
		sub++;
		amount++;
	}
}
void totalscore() {
	for (int loop = 0; loop < amount; loop++)
		stu[loop].total = stu[loop].scorem + stu[loop].scorep
			+ stu[loop].scorec;
}
void sortscore() {
	sort(stu, stu + amount, cmp);
}
void print() {
	for (int loop = 0; loop < amount; loop++)
		cout << stu[loop].id << ' ' << stu[loop].name << ' '
			<< stu[loop].scorem << ' '
			<< stu[loop].scorep << ' '
			<< stu[loop].scorec << ' '
			<< stu[loop].total << endl;
}
int main() {
	input();
	totalscore();
	sortscore();
	print();
	return 0;
}
