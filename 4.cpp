#include <iostream>
#include <cmath>
using namespace std;
int main() {
	unsigned long long num, ans = 0, bak;
	short sum = 0;
	cin >> num;
	bak = num;
	do {
		sum ++;
		num /= 10;
	} while (num);
	num = bak;
	do {
		ans += pow(num % 10, sum);
		num /= 10;
	} while (num);
	if (ans == bak) cout << "Yes";
	else cout << "No";
	return 0;
}
