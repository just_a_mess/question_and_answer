#include <iostream>
template <typename T> struct node {
	T data;
	node *next;
};
template <typename T> class list {
	node<T> *node_first;
	bool initialized;
	public:
		list() {
			node_first = new node<T>;
			node_first -> next = NULL;
			initialized = false;
		}
		void add(T data) {
			if (!initialized) {
				node_first -> data = data;
				initialized = true;
			}
			else {
				node<T> *node_new = new node<T>;
				node_new -> data = data;
				if (node_first -> data >= data) {
					node_new -> next = node_first;
					node_first = node_new;
				}
				else {
					node<T> *node_current = node_first;
					while (node_current -> next != NULL)
						if (node_current -> next -> data <= data)
							node_current = node_current -> next;
						else break;
					node_new -> next = node_current -> next;
					node_current -> next = node_new;
				}
			}
		}
		void output() {
			node<T> *node_current = node_first;
			while (node_current != NULL) {
				std::cout << node_current -> data << ' ';
				node_current = node_current -> next;
			}
		}
};
bool check(int number) {
	if (number <= 1) return 0;
	for (int loop = 2; loop * loop <= number; loop++)
		if (number % loop == 0) return 0;
	return 1;
}
int main() {
	list<int> linked_list;
	int number;
	while (std::cin >> number)
		if (check(number)) linked_list.add(number);
	linked_list.output();
	return 0;
}
